package com.apress.todo.todoauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoAuth2Application {

	public static void main(String[] args) {
		SpringApplication.run(TodoAuth2Application.class, args);
	}

}
