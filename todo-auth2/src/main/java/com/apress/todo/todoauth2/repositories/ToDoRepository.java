package com.apress.todo.todoauth2.repositories;

import com.apress.todo.todoauth2.domain.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;



/**
 * Class ToDoRepository
 */
@Repository
public interface ToDoRepository extends JpaRepository<ToDo, Long> , JpaSpecificationExecutor<ToDo> {
}
