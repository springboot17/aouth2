package com.apress.todo.todoauth2.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 *
 */
@Data
@Entity
@NoArgsConstructor
public class ToDo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @Column(insertable = true, updatable = false)
    private LocalDateTime created;
    private LocalDateTime modif;

    @PrePersist
    public void modCreated() {
        this.created = LocalDateTime.now();
    }

    @PreUpdate
    public void modUpdated() {
        this.modif = LocalDateTime.now();
    }
}
